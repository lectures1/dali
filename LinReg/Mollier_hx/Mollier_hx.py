#! /usr/bin/env python

""" create random points around isotherme and show least squares

    \draw[blue] (0.0, 40.24) -- (48.90554362272481, 44.01550796767436);

"""

import random
from matplotlib import pyplot as plt

# parameters

x1 = (0.0, 40.24)
x2 = (48.90554362272481, 44.01550796767436)

a0 = x1[1]
a1 = (x2[1] - x1[1])/(x2[0] - x1[0])
s  = 8                         # standard deviation

# generate data
random.seed(42)

# xs = [ 5, 15, 25, 35, 45 ]
xs = [ i*5 for i in range(1, 10) ]
ys = [ a0 + a1*x for x in xs ]
rs = [ random.normalvariate(mu=0, sigma=s) for _ in ys ]

# output in TikZ format
print()                         # clean line without prompt
for x, y, r in zip(xs, ys, rs):
    print(f"\\node [dot] at ({x}, {y + r/4}) {{}};")

print()                         # clean line without prompt
for x, y, r in zip(xs, ys, rs):
    print(f"\\node [dot] at ({x}, {y + r}) {{}};")

print()    
for x, y, r in zip(xs, ys, rs):
    print(f"\\draw ({x}, {y}) -- ({x}, {y + r});")
    print(f"\\node [dot] at ({x}, {w}) {{}};")

print()
print("squared distances")
for x, y, r in zip(xs, ys, rs):
    print(f"\\filldraw ({x}, {y+r}) -- ({x-r/2}, {y+r}) -- ({x-r/2}, {y}) -- ({x}, {y}) -- cycle;")
    print(f"\\node [dot] at ({x}, {y+r}) {{}};")
    
# plot data
plt.clf()
plt.ylim(0, 60)

plt.plot(*zip(x1, x2), color='black')
plt.plot(xs, vs, 'ob')
plt.plot(xs, ws, '.k')

# plt.show()


