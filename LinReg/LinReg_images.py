#! /usr/bin/env python3
# -*- coding: utf-8 -*-

""" generate images for linear regression technique """

import numpy as np

import scipy

from matplotlib import pyplot as plt
from matplotlib import cm
from matplotlib.animation import FuncAnimation, ImageMagickFileWriter, FFMpegWriter

from find_minimum import res_orthogonal_simple, res_orthogonal_squared, \
                         res_ordinate_simple, res_ordinate_squared

def nearest_point(b0, b1, P):
    """ find point on line y = b0 + b1⋅x nearest to P """

    A = np.array([0, b0])
    B = np.array([1, b0 + b1])
    n = B - A
    v = P - A
    t = np.dot(v, n)/np.dot(n, n)
    return A + t*(B - A)

def rotate_vector(P, angle=90):
    θ = np.deg2rad(angle)
    x1, y1 = P
    x2 = x1*np.cos(θ) - y1*np.sin(θ)
    y2 = x1*np.sin(θ) + y1*np.cos(θ)

    return x2, y2


def rotate_line(A, B, angle=90):
    Q = rotate_vector(B - A)

    return A + Q


def build_square(A, B):
    """ construct square on points A and B """

    C = rotate_line(A, B)
    D = C + B - A

    return A, B, D, C, A


def line_from_points(*points, **kwargs):
    xs = list(map(lambda x: x[0], points))
    ys = list(map(lambda x: x[1], points))

    return plt.Line2D(xs, ys, **kwargs)


def line_from_parameters(b0, b1, xrange=(0, 10), **kwargs):
    y = list(map(lambda x: b0 + b1*x, xrange))

    return plt.Line2D(xrange, y, **kwargs)

# figure 0
fig, ax = plt.subplots(figsize=(5,3))

ax.set_xlim(0, 1)
ax.set_ylim(0, 2)
ax.set_xlabel("x")
ax.set_ylabel("y")

x = 0.05 + 0.90*np.random.random(10)
y = 0.5 + 1.1*x + np.random.normal(scale=0.1, size=len(x))

ax.plot(x, y, "o")
fig.savefig('lr_00.png')
plt.close()

# figure 1
# sample data
x = np.array([1, 4, 8])
y = np.array([-2, 5, 6])

# line parameters
b0, b1 = (-6.2/3, 1.2)
lines = [line_from_parameters(b0, b1, color='r'),
         line_from_parameters(*res_orthogonal_simple.x, color='b', linestyle='dashed'),
         line_from_parameters(*res_orthogonal_squared.x, color='b'),
         line_from_parameters(*res_ordinate_simple.x, color='r', linestyle='dashed'),
         line_from_parameters(*res_ordinate_squared.x, color='r')
]
line = lines[0]


# fig, axs = plt.subplots(2, 2, sharex=True, sharey=True, figsize=(6,4))
fig, axs = plt.subplots(1, 4, sharex=True, sharey=True, figsize=(8,5))
# fig.suptitle("What regression line?")

# for row in axs:
# for ax in row:
for ax in axs:
    ax.set_xlim(0, 10)
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_aspect(1)

# axs[0][0].scatter(x, y, color='k', label='orthogonal simple')
# axs[0][1].scatter(x, y, color='k', label='orthogonal squared')
# axs[1][0].scatter(x, y, color='k', label='y-distance simple')
# axs[1][1].scatter(x, y, color='k', label='y-distance squared')
axs[0].scatter(x, y, color='k', label='orthogonal simple')
axs[1].scatter(x, y, color='k', label='orthogonal squared')
axs[2].scatter(x, y, color='k', label='y-distance simple')
axs[3].scatter(x, y, color='k', label='y-distance squared')

# axs[0][0].add_line(lines[1])
# axs[0][1].add_line(lines[2])
# axs[1][0].add_line(lines[3])
# axs[1][1].add_line(lines[4])
axs[0].add_line(lines[1])
axs[1].add_line(lines[2])
axs[2].add_line(lines[3])
axs[3].add_line(lines[4])

# plt.legend()
fig.savefig('lr_01.png')
plt.close()


# figure 2 (orthogonal distances)

fig, (ax0, ax1) = plt.subplots(1, 2, sharex=True, sharey=True, figsize=(8,5))

for ax in (ax0, ax1):
    ax.set_xlim(0, 10)
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_aspect(1)
    ax.scatter(x, y, color='k')
    ax.add_line(line_from_parameters(b0, b1, color='b'))

ax0.title.set_text('orthogonal simple')
for Q in zip(x, y):
    P = nearest_point(b0, b1, Q)
    ax0.add_line(line_from_points(P, Q, color='k') )

ax1.title.set_text('orthogonal squared')
for Q in zip(x, y):
    P = nearest_point(b0, b1, Q)
    q = build_square(P, Q)
    ax1.add_line(line_from_points(*q, color='k'))

fig.savefig('lr_02.png')
plt.close()


# figure 3 (distances in y-direction)

fig, (ax0, ax1) = plt.subplots(1, 2, sharex=True, sharey=True, figsize=(8,5))

for ax in (ax0, ax1):
    ax.set_xlim(0, 10)
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.scatter(x, y, color='k')
    ax.add_line(line_from_parameters(b0, b1, color='r'))

ax0.title.set_text('y-direction simple')
for Q in zip(x, y):
    P = Q[0], b0 + b1*Q[0]
    ax0.add_line(line_from_points(P, Q, color='k'))

ax1.title.set_text('y-direction squared')
for Q in zip(x, y):
    P = np.array([Q[0], b0 + b1*Q[0]])
    q = build_square(P, Q)
    ax1.add_line(line_from_points(*q, color='k'))

fig.savefig('lr_03.png')
plt.close()

# figure 4 and 5  (distances in y-direction)

fig4, (ax4, ax4b) = plt.subplots(1, 2, sharey=True, figsize=(8,4))
fig5, (ax5, ax5b) = plt.subplots(1, 2, sharey=True, figsize=(8,4))

for ax in (ax4, ax5):
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_xlim(0, 10)
    ax.set_ylim(-2.5, 10)
    ax.set_aspect(1)
    ax.scatter(x, y, color='k')
    ax.add_line(line_from_parameters(b0, b1, color='r'))
    ax.title.set_text('regression line')

for ax in (ax4b, ax5b):
    ax.set_xlim(-4, 4)
    ax.set_xlabel("deviation")
    ax.set_ylabel("cost")
    ax.title.set_text('derivative of cost function')

xb = np.linspace(-4, 4, 100)
ax4b.plot(xb, np.abs(xb), 'r')
ax5b.plot(xb, np.square(xb), 'r')

for Q in zip(x, y):
    P = Q[0], b0 + b1*Q[0]
    ax4.add_line(line_from_points(P, Q, color='k'))

for Q in zip(x, y):
    P = np.array([Q[0], b0 + b1*Q[0]])
    q = build_square(P, Q)
    ax5.add_line(line_from_points(*q, color='k'))

fig4.savefig('lr_04.png')
fig5.savefig('lr_05.png')
plt.close()


# figure 6 (least squares animation)

fig, (ax1, ax2) = plt.subplots(1, 2)
fig.suptitle('linear regression (least squares)')

pivot = (np.mean(x), np.mean(y))
x2 = []                         # x data for axis 2 (b1_)
y2 = []                         # y data for axis 2 (cost)

def animate_lr(b1_, n=100):

    ax1.clear()
    ax1.set_xlim(0, 12)
    ax1.set_ylim(-4, 10)
    ax1.set_xlabel("x")
    ax1.set_ylabel("y")
    ax1.set_aspect(1)
    
    b0_ = pivot[1] - pivot[0]*b1_
    ax1.scatter(x, y, color='k')
    cost = 0
    for Q in zip(x, y):
        y_model = b0_ + b1_*Q[0]
        cost += (y_model - Q[1])**2
        P = np.array([Q[0], y_model])
        q = build_square(P, Q)
        ax1.add_line(line_from_points(*q, color='k'))
    
    line = line_from_parameters(b0_, b1_, xrange=(0, 15), color='r')
    ax1.add_line(line)


    x2.append(b1_)
    y2.append(cost)
    
    ax2.clear()
    ax2.set_xlim(0.5, 2.0)
    ax2.set_ylim(0, 25)
    ax2.set_xlabel('β₁')
    ax2.set_ylabel('cost')
    ax2.plot(x2, y2, c='gray')
    ax2.scatter(x2[-1], y2[-1], c='red')


# f6_animation = FuncAnimation(fig, animate_lr, 
#                              frames=np.arange(0.5, 1.9, 0.01), interval=1, repeat=False)
# f6_animation.save('lr_06.gif', writer=ImageMagickFileWriter())
# f6_animation.save('lr_06.avi', writer=FFMpegWriter(fps=20))

# plt.show()
plt.close()


# figure 7 (contour plot)

def cost(b0, b1, xs=x, ys=y):
    """ sum of squared deviations """
    s = 0
    for x, y in zip(xs, ys):
        d = b0 + b1*x - y
        s+= d**2
    return s

fig, ax = plt.subplots()
fig.suptitle('sum of squared distances')

ax.set_xlabel('β₀')
ax.set_ylabel('β₁')

b0s = np.linspace(-5, 3, 100)
b1s = np.linspace(0.5, 2.0, 100)

X, Y = np.meshgrid(b0s, b1s)
Z = np.array([(b0s + b1s + 2)**2 + (b0s + 4*b1s - 5)**2 + (b0s + 8*b1s - 6)**2 for b0s, b1s in zip(X, Y)])
Z.shape=(len(X), len(Y))

plt.jet()                       # set colormap
ax.contour(X, Y, Z, levels=[8.5, 9, 12, 20, 30, 50, 80, 120, 200])

fig.savefig('lr_07.png')
plt.close()

# figure 8 (surface plot)

fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
fig.suptitle('sum of squared distances')

ax.set_xlabel('β₀')
ax.set_ylabel('β₁')
ax.set_zlabel('cost')
ax.set_zlim(0, 200)

surf = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm, linewidth=0, antialiased=False, vmin=0, vmax=100)

fig.colorbar(surf, shrink=0.5, aspect=5)
fig.savefig('lr_08.png')
plt.close()

# figure 9 (best fittings)
# a) least squares (numpy.linalg.lstsq)

fig9, ax9 = plt.subplots(figsize=(8,5))

ax9.set_xlim(0, 10)
ax9.set_xlabel("x")
ax9.set_ylabel("y")
ax9.scatter(x, y, color='k')

β1 = np.sum((y - np.mean(y))*x)/np.sum((x - np.mean(x))*x)
β0 = np.mean(y) - β1*np.mean(x)

for Q in zip(x, y):
    P = np.array([Q[0], β0 + β1*Q[0]])
    ax9.add_line(line_from_points(P, Q, color='k'))

ax9.add_line(line_from_parameters(β0, β1, color='r'))

pivot = list(map(np.mean, (x, y)))
ax9.plot(*pivot, 'ro')
ax9.annotate(r"($\bar{x}, \bar{y})$", 
             xy=pivot, xycoords='data',
             xytext=(0, -5), textcoords='offset points',
             # arrowprops=dict(facecolor='gray', shrink=0.05),
             horizontalalignment='left', verticalalignment='top')
    

fig9.savefig('lr_09.png')
plt.close()

# figure 10: residual plots

fig10, (ax10a, ax10b)  = plt.subplots(1, 2, sharex=True, sharey=True, figsize=(8,5))
plt.subplots_adjust(hspace=0.25)

ax10a.title.set_text('regression line')
ax10a.set_xlabel("x")
ax10a.set_ylabel("y")
ax10a.scatter(x, y, color='k')

# for Q in zip(x, y):
#     P = np.array([Q[0], β0 + β1*Q[0]])
#     ax10a.add_line(line_from_points(P, Q, color='k'))

ax10a.add_line(line_from_parameters(β0, β1, color='r'))

y_hat = β0 + β1*x             
residuals = y - y_hat           

ax10b.title.set_text('residual plot')
ax10b.set_xlim(0, 10)
ax10b.set_xlabel("x")
ax10b.set_ylabel(r"$y - \hat{y}$")
ax10b.scatter(x, residuals, color='r')
ax10b.plot((0, 10), (0, 0), 'k')

fig10.savefig('lr_10.png')
plt.close()

# figure 11: confidence interval of regression function
from scipy.stats import t

def confidence_interval(x0, x, b0, b1, alpha=0.05, upper=True):
    n = len(x)
    xm = np.mean(x)
    s = np.std(x)
    ssq_x = x.dot(x)
    t_krit = t.ppf(1 - alpha/2, df=n - 2)
    se = s*np.sqrt(1/n + (x0 - xm)**2/ssq_x)
    sem = t_krit*se
    y_hat = b0 + b1*x0
    sign = 1 if upper else -1
    return y_hat + sign*sem

fig11, ax11 = plt.subplots(figsize=(8,5))

ax11.set_xlabel("x")
ax11.set_ylabel("y")

rng = np.random.default_rng()
n = 20
x = 10 * rng.random(n)
y = 2 + 1.2*x + rng.normal(scale=1, size=n)

β1 = np.sum((y - np.mean(y))*x)/np.sum((x - np.mean(x))*x)
β0 = np.mean(y) - β1*np.mean(x)

ax11.scatter(x, y, color='k')

# for Q in zip(x, y):
#     P = np.array([Q[0], β0 + β1*Q[0]])
#     ax11.add_line(line_from_points(P, Q, color='k'))

xx = np.linspace(0, 10, 101)
cu = [ confidence_interval(x0, x, β0, β1, upper=True) for x0 in xx ]
cl = [ confidence_interval(x0, x, β0, β1, upper=False) for x0 in xx ]

ax11.plot(xx, β0 + β1*xx, color='r', label="regression line")
ax11.plot(xx, cu, color='blue', label="95% confidence interval")
ax11.plot(xx, cl, color='blue')

plt.legend()
fig11.savefig('lr_11.png')
plt.close()


# figure 12: confidence interval of regression function

def prediction_interval(x0, x, b0, b1, alpha=0.05, upper=True):
    n = len(x)
    xm = np.mean(x)
    s = np.std(x)
    ssq_x = x.dot(x)
    t_krit = t.ppf(1 - alpha/2, df=n - 2)
    se = s*np.sqrt(1 + 1/n + (x0 - xm)**2/ssq_x)
    sem = t_krit*se
    y_hat = b0 + b1*x0
    sign = 1 if upper else -1
    return y_hat + sign*sem

fig12, ax12 = plt.subplots(figsize=(8,5))

ax12.set_xlabel("x")
ax12.set_ylabel("y")

ax12.scatter(x, y, color='k')

# for Q in zip(x, y):
#     P = np.array([Q[0], β0 + β1*Q[0]])
#     ax12.add_line(line_from_points(P, Q, color='k'))

pu = [ prediction_interval(x0, x, β0, β1, upper=True) for x0 in xx ]
pl = [ prediction_interval(x0, x, β0, β1, upper=False) for x0 in xx ]

ax12.plot(xx, β0 + β1*xx, color='r', label="regression line")
ax12.plot(xx, cu, color='blue', label="95% confidence interval")
ax12.plot(xx, cl, color='blue')

ax12.plot(xx, pu, color='darkorchid', label="95% prediction interval")
ax12.plot(xx, pl, color='darkorchid')

plt.legend()
fig12.savefig('lr_12.png')
plt.close()

