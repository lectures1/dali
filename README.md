# TH Köln &mdash; Data Literacy Initiative

## Basismodul

- Lineare Regression: https://lectures1.gitlab.io/dali/LinearRegression/
- Hypothesentests:    https://lectures1.gitlab.io/dali/Hypothesentests.ipynb
