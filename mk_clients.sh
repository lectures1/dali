#! /usr/bin/env bash

for file in "LinReg/Lineare_Regression"
do
    sed -e "s/secret: '16398473588335682668',/secret: null,/" -e "s/glitch\.me\/master\.js/glitch\.me\/client\.js/" ${file}.html > ${file}-client.html
    diff -u ${file}.html ${file}-client.html > ${file}-client.patch
done
